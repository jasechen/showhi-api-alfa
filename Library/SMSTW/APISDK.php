<?php

/**
 * Call Rest Util
 *
 * @author Harry Wu<harry.wu@psycomputing.com>
 */
class SMSTW_APISDK {

    private $_api_key;

    public function __construct($api_key) {
        $this->_api_key = $api_key;
    }

    public function sendSMS($telphone,$content)
    {

        //$sendResult = self::_doGet('http://ec2-52-86-192-26.compute-1.amazonaws.com/SMSAPIServer/SMSSendAPI/Send/telphone/' . $telphone . '/content/' . urlencode($content) . '/useServiceName/SMSGO',$this->_api_key);
        $sendResult = self::_doGet('http://ec2-52-86-192-26.compute-1.amazonaws.com/SMSAPIServer/SMSSendAPI/Send/telphone/' . $telphone . '/content/' . urlencode($content) . '/useServiceName/URSMS',$this->_api_key);

        if($sendResult['code'] == 200)
        {
            return json_decode($sendResult['result'],true);
        }
        else
        {
            return array("success"=>false,"msg"=>"Request API Failed.","data"=>"");
        }

    }

    public function sendQuerySMSLog($param)
    {

        $sendResult = self::_doPost('http://ec2-52-86-192-26.compute-1.amazonaws.com/SMSAPIServer/SMSQueryAPI/QueryLog/',$param,$this->_api_key);

        if($sendResult['code'] == 200)
        {
            return json_decode($sendResult['result'],true);
        }
        else
        {
            return array("success"=>false,"msg"=>"Request API Failed.","data"=>"");
        }

    }

    static private function _doGet($url,$api_key) 
    {
        $headers = array(
            'api_key:' . $api_key,
            'Content-Type: application/json',
            'Expect:100-continue'
        );
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        $response = (String) curl_exec($handle);
        $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        return array('result' => $response , 'code' => $code);
    }

    static private function _doPost($url, $data,$api_key) 
    {
        $toURL = $url;
        $headers = array(
            'api_key:' . $api_key,
        );
        $ch = curl_init();
        $options = array(
            CURLOPT_URL=>$toURL,
            CURLOPT_HTTPHEADER=>$headers,
            CURLOPT_HEADER=>0,
            CURLOPT_VERBOSE=>0,
            CURLOPT_RETURNTRANSFER=>true,
            CURLOPT_POST=>true,
            CURLOPT_POSTFIELDS=>http_build_query($data),
        );
        curl_setopt_array($ch, $options);
        // CURLOPT_RETURNTRANSFER=true 會傳回網頁回應,
        // false 時只回傳成功與否
        $result = (String)curl_exec($ch); 
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return array('result' => $result , 'code' => $code);

    }
}
?>