<?php
/**
 * 请在下面放置任何您需要的应用配置
 */

return array(

  'hosts' => array(
    'mQ_CDu1XTf-g2C_KFzO87g' => array(
        'id' => 'mQ_CDu1XTf-g2C_KFzO87g',
        'email' => 'showhi_a01@showhi.co',
        'start_date' => '2017-09-12',
        'end_date' => '2018-09-12',
    ),
    'WYB0SNRoQfSs1qrFgd5jkg' => array(
        'id' => 'WYB0SNRoQfSs1qrFgd5jkg',
        'email' => 'showhi_a02@showhi.co',
        'start_date' => '2017-09-12',
        'end_date' => '2018-09-12',
    ),
    '145b09d78715823f997e8f20564ec4' => array(
        'id' => '145b09d78715823f997e8f20564ec4',
        'email' => 'showhi_a03@showhi.co',
        'start_date' => '2017-09-12',
        'end_date' => '2018-09-12',
    ),
    '158131d82f7c41c88a873781631dea' => array(
        'id' => '158131d82f7c41c88a873781631dea',
        'email' => 'showhi_a04@showhi.co',
        'start_date' => '2017-09-12',
        'end_date' => '2018-09-12',
    ),
    '1181598a81f253bb3686d5460c4b33' => array(
        'id' => '1181598a81f253bb3686d5460c4b33',
        'email' => 'showhi_a05@showhi.co',
        'start_date' => '2017-09-12',
        'end_date' => '2018-09-12',
    ),
    'b9ee5ea37d7202b3bccf4140d5fd41' => array(
        'id' => 'b9ee5ea37d7202b3bccf4140d5fd41',
        'email' => 'showhi_a06@showhi.co',
        'start_date' => '2017-09-12',
        'end_date' => '2018-09-12',
    ),
    '1dae678dede6c084e5bff628b2eae9' => array(
        'id' => '1dae678dede6c084e5bff628b2eae9',
        'email' => 'showhi_a07@showhi.co',
        'start_date' => '2017-09-12',
        'end_date' => '2018-09-12',
    ),
    '6140b4bf3415a4f8d93e05ddb8ccda' => array(
        'id' => '6140b4bf3415a4f8d93e05ddb8ccda',
        'email' => 'showhi_a08@showhi.co',
        'start_date' => '2017-09-12',
        'end_date' => '2018-09-12',
    ),
    'b9142a0cc28f1350660138b82d73d1' => array(
        'id' => 'b9142a0cc28f1350660138b82d73d1',
        'email' => 'showhi_a09@showhi.co',
        'start_date' => '2017-09-12',
        'end_date' => '2018-09-12',
    ),
    '36a711c6c2cb6764b77a11cbdf97f6' => array(
        'id' => '36a711c6c2cb6764b77a11cbdf97f6',
        'email' => 'showhi_a10@showhi.co',
        'start_date' => '2017-09-12',
        'end_date' => '2018-09-12',
    ),
  ),

);
