<?php
/**
 * $APP_NAME 统一入口
 */
 
 //不是這個檔案

require_once dirname(__FILE__) . '/init.php';

//装载你的接口
DI()->loader->addDirs('Demo');

//使用json api方式
$HTTP_RAW_POST_DATA = file_get_contents('php://input');
$HTTP_RAW_POST_DATA = $HTTP_RAW_POST_DATA ? $HTTP_RAW_POST_DATA : "{}";
    //合併變數到陣列裡面
DI()->request = new PhalApi_Request(array_merge($_REQUEST,json_decode($HTTP_RAW_POST_DATA, true))); 


//end 使用json api方式

/** ---------------- 响应接口请求 ---------------- **/

$api = new PhalApi();
$rs = $api->response();
$rs->output();

