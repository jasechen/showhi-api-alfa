<?php









return array(
    'Hello {name}, Welcome to use PhalApi!' => 'Hello {name}, Welcome to use PhalApi!',
    'wrong sign' => 'wrong sign',
    'user not exists' => 'user not exists',

    // msg
    'NoCheckEmail' => 'Your mailbox have not verified, please go to the reception for verification',
    'NoCheckPhone' => 'Your phone has not yet verified, please go to receive a message to verify',
    'NoPoint' => 'Your points after use',
    'NoNickName' => 'you has not filled out in English  nickname',
    'NoApproval' => 'Your account has not been open',
    'AccountError' => 'Your account has Error',
    'NoAccountError' => 'No Account Error',
    'SystemError' => 'System Error',
    'SystemErrorAgain' => 'System error, please re-do it again',
    'BefaultBooking' => '請在 30 小時前booking',
    'YouhaveLesson' => 'This time you have a course',
    'TeacherNoTime' => 'The teacher did not open this time',
    'LessonFullNumber' => 'The number of classes is full',
    'BookingSuccess' => 'Courses completed',
    'NotFindLesson' => 'I can not find this course',
    'BefaultStudnetHour' => 'Please return 6 hours before lesson',
    'CancelSuccess' => 'Course withdrawal is complete, click Back',
    'BefaultTeacherHour' => 'Please return 24 hours before lesson',

    'Notify_invitee' => 'Has been invited and registered as a member of ShowHi.',
    'Wait_approval' => 'Wait for the invitee to Approval your account.',
    'QR_approval' => 'Your account has been Approval.',
    'Notify_modified_data' => "Your information is not yet complete <a href='User.TeacherEdit'>Modify the information</a> Improve your information.",


    //end  msg

//sms email
    'WakeupAccount' => 'Wakeup Account',
    'CodeError' => 'Verification code error',
    'AccountVA' => 'Account Verification Link',
    'AccountVANote' => 'Account is completed, remember to verify the mailbox and the phone to complete the account wake up<br>
        Account:{loginid}<br>
        
        Activate account link:
        <a href="{host_path}User.WakeupAccount/{user_id}/{email_code}">
        {host_path}User.WakeupAccount/{user_id}/{email_code}</a><br>

             Showhi Team',
    'EmailPhoneError' => 'Mailbox or phone is wrong',
    'ResetPasswordVA' => 'Reset Password Verification:',



    'CourseCancellationNoticeSubject' => 'Showhi Course Cancellation Notice',

    'StudentCourseCancellationNotice' => '<div class="shText-1">{subject}</div>
        <div class="m-l1 nameBox">Hi {nick_name},</div><br>
             Your course on  <a target="_blank" href="{time_con_link}">{booking_select_time}</a> <strong style="color:red;">(Taiwan Time)</strong> <br>
             will be cancelled, Sorry for the inconvenience caused.
             <br>
            <a target="_blank" href="{host_path}/User">Showhi</a>
             <br>
             Sincerely,
             <br><br>
             Showhi Team<br>',

    'SmsStudentCourseCancellationNotice' => '{subject}
             Hi {nick_name},
             Your course on  {booking_select_time} (Taiwan Time)
             will be cancelled, Sorry for the inconvenience caused.
             Sincerely,Showhi Team',

    'TeacherCourseCancellationNotice' => '<div class="shText-1">{subject}</div>
        <div class="m-l1 nameBox">Hi {nick_name},</div><br>
             Your course on  <a target="_blank" href="{time_con_link}">{booking_select_time}</a> <strong style="color:red;">(Taiwan Time)</strong> <br>
             will be cancelled, Sorry for the inconvenience caused.
             <br>
            <a target="_blank" href="{host_path}/User">Showhi</a>
             <br>
             Sincerely,
             <br><br>
             Showhi Team<br>',

    'SmsTeacherCourseCancellationNotice' => '{subject}
             Hi {nick_name},
             Your course on  {booking_select_time} (Taiwan Time)
             will be cancelled, Sorry for the inconvenience caused.
             Sincerely,Showhi Team',

    'EmailBookingSubject' => 'Showhi Appointment Notification',
    'EmailBooking' => '
        <div class="m-l1 nameBox">Dear {nick_name},</div><br>
        This is the reminder for your Showhi appointment<br>
        <b>Date:</b>{booking_select_time}<br>
        <b>Time:</b>{booking_select_hour} (Taiwan Time)<br>

        Go to <a target="_blank" href="{time_con_link}">The World Clock</a>.<br>

        If you have any problem keeping this appointment,please contact the<br>
        requester or invitee immidiately!<br><br>

        Thank you!<br><br>

        This Notification is sent by automatic system,please do not reply.<br><br>

        --Explore the world with Showhi
             ',
    'SmsEmailBooking' => '{subject}
        Hi {nick_name},
             Your Showhi class has been arranged for {booking_select_time} (Taiwan Time)
             Sincerely,Showhi Team',

    'EmailToStudentBooking' => '<div class="shText-1">{subject}</div>
        <div class="m-l1 nameBox">Hi {nick_name},</div><br>
             You have an appointment in courses <a target="_blank" href="{time_con_link}">{booking_select_time}</a> <strong style="color:red;">(Taiwan Time). </strong><br>
             Sincerely,
             <br><br>
             Showhi Team<br>
             ',
    'SmsEmailToStudentBooking' => '{subject} 
        Hi {nick_name},
             You have an appointment in courses {booking_select_time} (Taiwan Time)
             Sincerely,Showhi Team',



    'CronNoticeHourSubject' => 'Showhi notice: Your course will begin in {hour} hour(s). ',
    'CronNoticeHour' => '<div class="shText-1">{subject}</div>
        <div class="m-l1 nameBox">Hi {nick_name},</div><br>
                Your course will begin in {hour} hour(s). 
             <br><br>
             Sincerely,
             <br><br>
             Showhi Team<br>
             ',
    'SmsCronNoticeHour' => '{subject}
        Hi {nick_name},
        Your course will begin in {hour} hour(s). 
        Sincerely,Showhi Team',

//end sms emal

);
